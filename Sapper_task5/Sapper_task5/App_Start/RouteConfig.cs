﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sapper_task5
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

             routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }

            );
            routes.MapRoute(
                name: "GameIndex",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Sapper", action = "Index", id = UrlParameter.Optional }

            );
            routes.MapRoute(
              name: "Game",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "Sapper", action = "Game", id = UrlParameter.Optional }
          );

            routes.MapRoute(
               name: "GameGetGameState",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Sapper", action = "GetGameState", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "GameUserAction",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Sapper", action = "UserAction", id = UrlParameter.Optional }
            );
        }
    }
}