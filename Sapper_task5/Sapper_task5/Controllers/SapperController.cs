﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using  Sapper_task5.Models;
using  Sapper_task5.Sapper_Engine;

namespace Sapper_task5.Controllers
{
  public class SapperController : Controller
    {

        private SapperEngine Game
        {
            get
            {
                return (SapperEngine)Session["game"];
            }
            set
            {
                Session.Add("game", value);
            }
        }

        private Sapper sapper
        {
            get
            {
                return (Sapper)Session["sapper"];
            }
            set
            {
                Session.Add("sapper", value);
            }
        }

      //  private Sapper sapper;
        
        
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Sapper _sapper)
        {
            bool error = false;
            int _width=0;
            int _height=0;
            int _mine=0;
            sapper = _sapper;  
            TimeSpan _time; 
            _time = TimeSpan.Zero;
            try
            {
                 _width = sapper.Width;
                 _height = sapper.Height;
                 _mine = sapper.NumberOfMine;
                _time = sapper.TimeLimit;
                if (_width < 1 || _width > 100)
                    error = true;
                if (_height < 1 || _height > 100)
                    error = true;
                if (_mine < 1 || _mine > _width * _height - (_width * _height)/50)
                    error = true;       
            }
            catch (Exception e)
            {
                error = true;
                ViewBag.Result = (e);
            }
            if (error)
                ViewBag.Result += ("Invalid settings!");
            else
            {
                ViewBag.Result = "";
                Game=new SapperEngine();
                Game.SapperCreate(_width, _height, _mine, DateTime.Now + _time);               
            }
            return View();
        }

        [HttpPost]
        public void GetGameState()
        {
            try
            {
                if (Game.GetGameState == GameState.GameOver)
                Game.OpenAllMines();
                var model = GetModel();
                var jsonObject = JsonConvert.SerializeObject(model);
                Response.Write(jsonObject);
            }
            catch (Exception)
            {
                Response.Write("null");
            }           
        }

        public SapperEngineModel GetModel()
      {
            Cell[,] _matrix;
           _matrix = Game.GetGameMatrix();
          var matrix = new string[sapper.Height][];
          for (int i = 0; i < sapper.Height; i++)
          {
              matrix[i] = new string[sapper.Width];
              for (int j = 0; j < sapper.Width; j++)
              {
                  if (_matrix[j, i].IsFlag)
                  {
                      matrix[i][j] = SapperEngineModel.CellTypeForNamesUser[CellTypeForUser.IsFlag];
                      continue;
                  }
                  if (_matrix[j, i].Hide)
                  {
                      matrix[i][j] = SapperEngineModel.CellTypeForNamesUser[CellTypeForUser.Hide];
                  }
                  else
                  {
                      if (_matrix[j, i].Type == CellTypeInGame.Mine)
                      {
                          matrix[i][j] = SapperEngineModel.CellTypeNames[CellTypeInGame.Mine];
                          continue;
                      }
                      if (_matrix[j, i].Type == CellTypeInGame.NearMine)
                      {
                          matrix[i][j] = _matrix[j, i].NumberOfMine.ToString();
                          continue;
                      }
                      if (_matrix[j, i].Type == CellTypeInGame.Free)
                      {
                          matrix[i][j] = SapperEngineModel.CellTypeNames[CellTypeInGame.Free];                         
                      }
                  }
              }
          }
            DateTime _endTime;
            _endTime = Game.GetTimerActivStat ? Game.GetEndTime : DateTime.MaxValue;          
            var model = new SapperEngineModel
            {             
                Matrix = matrix,
                Width = sapper.Width,
                Height = sapper.Height,
                State = SapperEngineModel.GameStateNames[Game.GetGameState],
                EndTime = ((_endTime.Ticks - new DateTime(1970, 1, 1, 3, 0, 0).Ticks) / 10000)               
            };
            return model;         
      }


      [HttpPost]
        public void UserAction()
      {
          try
          {
               var userAction = new UserActionModel
               {
                  ActionName = Request.Form["ActionName"],
                  Col = Convert.ToInt32(Request.Form["Col"]),
                  Row = Convert.ToInt32(Request.Form["Row"]),
              };
               if (Game.IsNewGeme)
               Game.NewGame(userAction.Col, userAction.Row);
              if (Game.GetGameState != GameState.Game)
              {
                  GetGameState();
                  return;
              }            
              if (userAction.Action == UserActionModel.ActionEnum.Left)
                  Game.ClickCell(userAction.Row, userAction.Col, Click.Left);

              if (userAction.Action == UserActionModel.ActionEnum.Right)
                  Game.ClickCell(userAction.Row, userAction.Col, Click.Right);

              GetGameState();
          }
          catch (Exception)
          {
              return;            
          }       
      }


    }

}
