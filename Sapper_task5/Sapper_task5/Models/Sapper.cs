﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sapper_task5.Models
{
    public class Sapper
    {
        public int Width
        { get; set; }

        public int Height
        { get; set; }

        public int NumberOfMine
        { get; set; }
        public bool IsTimeLimit
        {
            get
            {
                return TimeLimit.Ticks > 0;
            }
        }
        public TimeSpan TimeLimit
        { get; set; }


    }
}