﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Sapper_task5.Sapper_Engine;

namespace Sapper_task5.Models
{
    public class SapperEngineModel
    {
        [JsonIgnore]
        public static readonly Dictionary<CellTypeInGame, string> CellTypeNames = new Dictionary<CellTypeInGame, string>()
        {
            {CellTypeInGame.NearMine, "NearMine"},
            {CellTypeInGame.Mine, "Mine"},
            {CellTypeInGame.Free, "Free"}

        };
        
         [JsonIgnore]
         public static readonly Dictionary<CellTypeForUser, string> CellTypeForNamesUser= new Dictionary<CellTypeForUser, string>()
        {
            {CellTypeForUser.IsFlag, "IsFlag"},
            {CellTypeForUser.Hide, "Hide"}
        };

         
        [JsonIgnore]
        public static readonly Dictionary<GameState, string> GameStateNames = new Dictionary<GameState, string>()
        {
            {GameState.Game , "Game"},
            {GameState.Victory, "Victory"},
            {GameState.GameOver, "Gameover"}
        }; 
        
        public string[][] Matrix { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }              
        public string State { get; set; }
        public long EndTime { get; set; }            
    }

}