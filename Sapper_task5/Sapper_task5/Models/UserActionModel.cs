﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sapper_task5.Models
{
    public class UserActionModel
    {
        public enum ActionEnum
        {
            Left,
            Right,          
            Invalid
        }

        [JsonIgnore]
        public static readonly Dictionary<string,ActionEnum > ActionNames = new Dictionary<string, ActionEnum>()
        {
            {"left",ActionEnum.Left },
            {"right",ActionEnum.Right}
        };
        [JsonIgnore]
        public ActionEnum Action
        {
            get
            {
                return ActionNames.ContainsKey(ActionName) ? ActionNames[ActionName] : ActionEnum.Invalid;
            }
        }
        public string ActionName { get; set; }
        public int Col { get; set; }
        public int Row { get; set; }

    }
}