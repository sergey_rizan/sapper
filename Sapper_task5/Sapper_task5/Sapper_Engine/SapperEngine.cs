﻿using System;
using System.Collections.Generic;


namespace Sapper_task5.Sapper_Engine
{
    public enum GameState
    {
        Game,
        Victory,
        GameOver
    }
    public class Cell
    {
        public CellTypeInGame Type
        {
            get;
            set;
        }

        public bool IsFlag
        {
            get;
            set;
        }

        public bool Hide
        {
            get;
            set;
        }

        public int NumberOfMine
        {
            get;
            set;
        }       
    }
    public enum CellTypeForUser
    {
        IsFlag,
        Hide
    }
    public enum Click
    {
        Right,
        Left
    }
    public enum CellTypeInGame
    {
        Mine,
        NearMine,
        Free
    }
    class SapperEngine
    {
        Cell[,] _gameMatrix;
        int _width;
        int _height;
        int _minesNumber;
        bool _timerState;
        GameState _gameState;
        DateTime _endTime;
        DateTime _startTime;
        public Cell GetCell(int col, int row)
        {
            return (_gameMatrix[col, row]);
        }

        public Cell[,] GetGameMatrix()
        {
            return (_gameMatrix);
        }
        public DateTime GetStarTime
        {
            get
            {
                return _startTime;
            }
        }
        public DateTime GetEndTime
        {
            get
            {
                return _endTime;
            }
        }

        public bool GetTimerActivStat
        {
            get { return _timerState; }
        }
        public bool IsNewGeme
        {
            get { return _isNewGeme; }
        }

        bool _isNewGeme;
        public GameState GetGameState
        {
            get
            {
                if (_timerState && _endTime.Ticks - DateTime.Now.Ticks < 0)
                    _gameState = GameState.GameOver;
                return _gameState;
            }
        }

        public void SapperCreate(int width, int heigth, int mines, DateTime time)
        {
            _startTime = DateTime.Now;
            _endTime = time;
            _minesNumber = mines;
            _width = width;
            _height = heigth;
            _gameMatrix = new Cell[_width, _height];
            _gameState = GameState.Game;            
            _timerState = Math.Truncate (_endTime.TimeOfDay.TotalSeconds)!= Math.Truncate (_startTime.TimeOfDay.TotalSeconds);

            for (int i = 0; i < _height; i++)
                for (int j = 0; j < _width; j++)
                {
                    _gameMatrix[j, i] = new Cell();
                    _gameMatrix[j, i].Hide = true;
                    _gameMatrix[j, i].IsFlag = false;
                    _gameMatrix[j, i].NumberOfMine = 0;
                    _gameMatrix[j, i].Type = CellTypeInGame.Free;
                }
            _isNewGeme = true;
        }
        // generation new game
        public void NewGame(int col, int row)
        {
            _gameState = GameState.Game;
            var _rand = new Random();
            int _mine = 0;
            while (_mine < _minesNumber)
            {
                int i = _rand.Next(_width);
                int j = _rand.Next(_height);
                if (_gameMatrix[j, i].Type != CellTypeInGame.Mine && (col != i || row != j))
                {
                    _gameMatrix[j, i].Type = CellTypeInGame.Mine;
                    _gameMatrix[j, i].NumberOfMine = 10;
                    _mine++;
                    MineCount(j, i);
                }
            }
            _isNewGeme = false;
        }
        //add mine in fild
        private void MineCount(int col, int row)
        {
            for (int i = row - 1; i <= row + 1; i++)
            {
                if (i < 0)
                    i = 0;
                if (i < _height)
                {
                    for (int j = col - 1; j <= col + 1; j++)
                    {
                        if (j < 0)
                            j = 0;
                        if (j < _width)
                        {
                            if (_gameMatrix[j, i].Type != CellTypeInGame.Mine)
                            {
                                _gameMatrix[j, i].NumberOfMine++;
                                _gameMatrix[j, i].Type = CellTypeInGame.NearMine;
                            }
                        }
                    }
                }
            }
        }
        //user action
        public void ClickCell(int col, int row, Click click)
        {
            if (click == Click.Left && _gameMatrix[col, row].Hide)
            {
                _gameMatrix[col, row].Hide = false;
                if (_gameMatrix[col, row].Type == CellTypeInGame.Mine)
                {
                    _gameState = GameState.GameOver;
                }
                if (_gameMatrix[col, row].Type == CellTypeInGame.Free)
                {
                    CellsOpen(col, row);
                }               
            }
            if (click == Click.Right)
            {
                if (!_gameMatrix[col, row].IsFlag && _gameMatrix[col, row].Hide)
                {                    
                    _gameMatrix[col, row].IsFlag = true;
                    _gameMatrix[col, row].Hide = false;
                }
                else if (!_gameMatrix[col, row].Hide && _gameMatrix[col, row].IsFlag)
                {
                    _gameMatrix[col, row].IsFlag = false;
                    _gameMatrix[col, row].Hide = true;
                }
                else if (!_gameMatrix[col, row].Hide && _gameMatrix[col, row].Type == CellTypeInGame.NearMine)
                {
                    OpenCellsNearMine(col, row);

                }
            }
            ChekGameVictory();
        }
        private bool CheckCellsNearMine(int col, int row)
        {
            int _mineCounter=0;
            int _flagCounter=0;
            if (_gameMatrix[col, row].Type == CellTypeInGame.NearMine && !_gameMatrix[col, row].Hide)
            {
                for (int i = row - 1; i <= row + 1; i++)
                {
                    if (i < 0)
                        i = 0;
                    if (i < _height)
                    {
                        for (int j = col - 1; j <= col + 1; j++)
                        {
                            if (j < 0)
                                j = 0;
                            if (j < _width)
                            {
                                if (_gameMatrix[j, i].Type == CellTypeInGame.Mine )                               
                                    _mineCounter++;
                                if (_gameMatrix[j, i].IsFlag)
                                    _flagCounter++;
                            }
                        }
                    }
                }
                if (_mineCounter == _flagCounter)
                return true;
            }
            return false;
        }

        private void OpenCellsNearMine(int col, int row)
        {
            if (CheckCellsNearMine(col, row))
            {
                for (int i = row - 1; i <= row + 1; i++)
                {
                    if (i < 0)
                        i = 0;
                    if (i < _height)
                    {
                        for (int j = col - 1; j <= col + 1; j++)
                        {
                            if (j < 0)
                                j = 0;
                            if (j < _width)
                            {
                                if (_gameMatrix[j, i].Hide)
                                {
                                    _gameMatrix[j, i].Hide = false;
                                }
                            }
                        }
                    }
                }
                
            }
            
        }
        private void CellsOpen(int col, int row)
        {          
            List<int[]> _list = new List<int[]>();

            _list.Add(new[] { col, row });

            while (_list.Count > 0)
            {
                int j = _list[0][0];
                int i = _list[0][1];
                _gameMatrix[j, i].Hide = false;
                _list.RemoveAll(l => !_gameMatrix[l[0], l[1]].Hide);
                for (int pi = i - 1; pi <= i + 1; pi++)
                    for (int pj = j - 1; pj <= j + 1; pj++)
                        if (0 <= pj && pj < _width && 0 <= pi && pi < _height)
                        {
                            if (_gameMatrix[pj, pi].Type == CellTypeInGame.Free)
                            {
                                if (_gameMatrix[pj, pi].Hide)
                                    _list.Add(new[] { pj, pi });
                            }
                            else
                                _gameMatrix[pj, pi].Hide = false;
                        }
            }
        }    
      
        public void OpenAllMines()
        {  
            for (int i = 0; i < _height; i++)
                for (int j = 0; j < _width; j++)
                {

                    if (_gameMatrix[j, i].Hide && _gameMatrix[j, i].Type == CellTypeInGame.Mine)
                    {
                        _gameMatrix[j, i].Hide = false;
                    }
                }
        }

        private void ChekGameVictory()
        {
            int _hideCounter = 0;
            int _hideCounterInMine = 0;
            int _flagCounter = 0;
            int _flagInMineCounter = 0;
            for (int i = 0; i < _height; i++)
                for (int j = 0; j < _width; j++)
                {
                    if (_gameMatrix[j, i].Hide)
                    {
                        _hideCounter++;
                        if (_gameMatrix[j, i].Type == CellTypeInGame.Mine)
                        {
                            _hideCounterInMine++;

                        }
                    }
                    if (_gameMatrix[j, i].IsFlag)
                    {
                        _flagCounter++;
                        if (_gameMatrix[j, i].Type == CellTypeInGame.Mine)
                        {
                            _flagInMineCounter++;
                        }
                    }
                }
            if (_gameState == GameState.Game &&
                ((_hideCounter == _minesNumber && _hideCounterInMine == _hideCounter) || 
                (_flagInMineCounter == _minesNumber && _flagCounter == _flagInMineCounter)||
                (((_hideCounterInMine + _flagInMineCounter) == (_hideCounter + _flagCounter))&&
                ((_hideCounter + _flagCounter)==_minesNumber))))
            {
                _gameState = GameState.Victory;                
            }
        }
       
    }

}