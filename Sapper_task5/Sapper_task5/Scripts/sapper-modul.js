﻿$(window).load(function () {

    var cellTypeInGames = {
        flag: "IsFlag",
        hide: "Hide",
        mine: "Mine",
        free: "Free",
        nearmine: "NearMine"
    };

    var cellTypeInGameAttr = {};
    cellTypeInGameAttr[cellTypeInGames.flag] = "IsFlag";
    cellTypeInGameAttr[cellTypeInGames.hide] = "Hide";
    cellTypeInGameAttr[cellTypeInGames.mine] = "Mine";
    cellTypeInGameAttr[cellTypeInGames.free] = "Free";
    cellTypeInGameAttr[cellTypeInGames.nearmine] = "NearMine";

    var actions = {
        Left: "left",
        Right: "right"
    };

    var gameStates = {
        game: "Game",
        gameOver: "GameOver",
        victory: "Victory"
    };

    var gameStatesAttr = {}
    gameStatesAttr[gameStates.game] = "Game";
    gameStatesAttr[gameStates.gameOver] = "Gameover";
    gameStatesAttr[gameStates.victory] = "Victory";

    var gameMessages = {};
    gameMessages[gameStates.gameOver] = "You lose...";
    gameMessages[gameStates.victory] = "You win!!!";
    gameMessages["timelimit"] = "Remaining time: {0}.";


    var endTime = null;
    var timer = null;

    //Helpers------------------------------------------------------
    var getTimeDifference = function (earlierDate, laterDate) {
        var oDiff = {};

        var nTotalDiff = Math.max(laterDate.getTime() - earlierDate.getTime(), 0);

        var msec = nTotalDiff;
        oDiff.total = nTotalDiff;
        oDiff.h = Math.floor(msec / 1000 / 60 / 60);
        msec -= oDiff.h * 1000 * 60 * 60;
        oDiff.m = Math.floor(msec / 1000 / 60);
        msec -= oDiff.m * 1000 * 60;
        oDiff.s = Math.floor(msec / 1000);
        msec -= oDiff.s * 1000;
        oDiff.ms = msec;

        return oDiff;
    }

    var stringFormat = function () {
        var s = arguments[0];
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, arguments[i + 1]);
        }

        return s;
    }

    var convertTimeDifferenceToString = function (timeDiff) {
        if (timeDiff.h > 10000)
            return "unlimited";

        var m = timeDiff.m;
        var s = timeDiff.s;
        if (m < 10)
            m = "0" + m;
        if (s < 10)
            s = "0" + s;
        return stringFormat("{0}:{1}:{2}", timeDiff.h, m, s);
    }
    //-------------------------------------------------------------


    //Ajax-Requests------------------------------------------------
    var getGameState = function (successHandler) {
        $.ajax({
            type: "POST",
            url: "/Sapper/GetGameState",
            success: function (resp) {
                successHandler(resp);
            },
            dataType: "json"
        });
    };

    var sendUserAction = function (sendedObject, successHandler) {
        $.ajax({
            type: "POST",
            url: "/Sapper/UserAction",
            success: function (resp) {
                successHandler(resp);
            },
            data: sendedObject,
            dataType: "json"
        });
    };
    //------------------------------------------------------------


    //Handlers----------------------------------------------------
    var serverModelHandler = function (data) {
        $(".sapper-area").html("");

        buildMineField(data, $(".sapper-area"));

        $(".sapper-message")
            .removeAttr(gameStates.game)
            .attr(gameStatesAttr[data.State], 0)
            .text(stringFormat(gameMessages[data.State]));

        if (data.State == gameStates.gameOver || data.State == gameStates.victory)
            clearInterval(timer);

        endTime = new Date(+data.EndTime);
    };

    var timerHandler = function () {
        var currentTime = new Date();
        var timeDiff = getTimeDifference(currentTime, endTime);

        if (timeDiff.total == 0) {
            clearInterval(timer);
            getGameState(serverModelHandler);
        }

        var timeString = convertTimeDifferenceToString(timeDiff);

        $(".sapper-time").text(stringFormat(gameMessages["timelimit"], timeString));
    };
    var cellClickHandler = function (event) {
        var self = $(this);
        var act = actions.Left;
        if (event.ctrlKey)
            act = actions.Right;

        var action = {
            ActionName: act,
            Row: self.attr("Row"),
            Col: self.attr("Col")
        };

        sendUserAction(action, serverModelHandler);
    };
    //Sapper-Builder---------------------------------------------
    var buildMineField = function (serverModel, parent) {
        var matrix = serverModel.Matrix;
        var width = serverModel.Width;
        var height = serverModel.Height;     
        var table = $("<table>");
        for (var j = 0; j < width; j++) {
            var row = $("<tr>");

            for (var i = 0; i < height; i++) {
                var cell = $("<td>")
                    .attr("class", "sapper-cell")
                    .attr("Col", i)
                    .attr("Row", j);
                var d = (matrix[i][j]) ^ 0;
                    if (d != (matrix[i][j]))
                cell.attr(matrix[i][j].toString(),"0");
                else
                    cell.text(matrix[i][j]);               
                cell.click(cellClickHandler);
                row.append(cell);
            }
            table.append(row);
        }

        parent.append(table);
    };

    var init = function () {
        getGameState(serverModelHandler);
        timer = setInterval(timerHandler, 100);
    };

    init();
});